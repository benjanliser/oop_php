<?php

    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');
    

    $animal = new Animal ("Sapi");
    echo "Nama Binatang :" . $animal->name . "<br>";
    echo "Jumlah Kaki : " .  $animal->leg . "<br>";
    echo "Cold blooded : " . $animal->cold_blooded . "<br>";

    echo "<br>";

    $animal = new Animal ("Kambing");
    echo "Nama Binatang :" . $animal->name . "<br>";
    echo "Jumlah Kaki : " .  $animal->leg . "<br>";
    echo "Cold blooded : " . $animal->cold_blooded . "<br>";

    echo "<br>";


    $frog = new Frog("Buduk");
    echo "Nama Binatang :" . $frog->name . "<br>";
    echo "Jumlah Kaki : " .  $frog->leg . "<br>";
    echo "Cold blooded : " . $frog->cold_blooded . "<br>";
    echo $frog->Jump();

    echo "<br><br>";

    $ape = new Ape("Kera Sakti");
    echo "Nama Binatang :" . $ape->name . "<br>";
    echo "Jumlah Kaki : " .  $ape->leg . "<br>";
    echo "Cold blooded : " . $ape->cold_blooded . "<br>";
    echo $ape->Yell();



?>